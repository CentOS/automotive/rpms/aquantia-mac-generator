Name:		aquantia-mac-generator
Version:	1.2
Release:	3%{?dist}
Summary:	Correct Aquantia 10GB MAC address with trailing zeros
BuildArch:	noarch
License:	GPLv2
URL:		https://gitlab.com/CentOS/automotive/rpms/aquantia-mac-generator
Source0:	aquantia-mac-generator.service
Source1:	aquantia-mac-generator.sh

%description
Generates a systemd-networkd link file with a random MAC address based
on the board serial number for the 10GB Aquantia NICs that come up with
a MAC address of 00:17:b6:00:00:00.

%install
mkdir -p "%{buildroot}/lib/systemd/system/" "%{buildroot}/usr/bin/"
install %{SOURCE0} "%{buildroot}/lib/systemd/system/aquantia-mac-generator.service"
install %{SOURCE1} "%{buildroot}/usr/bin/"

%postun
rm -f /run/systemd/network/10-aquantia-10gb.link

%files
/lib/systemd/system/aquantia-mac-generator.service
/usr/bin/aquantia-mac-generator.sh

%changelog
* Fri Jan 5 2023 Brian Masney <bmasney@redhat.com> 1.2-3
- Simplify packaging to be more consistent with stmmac-mac-generator.

* Fri Jun 30 2023 Eric Chanudet <echanude@redhat.com> 1.2-2
- Fallback all the way to random MAC address attribution if no mehtod to
  generate the MAC worked and handle the absence of cmdline argument safely.

* Wed Dec 7 2022 Brian Masney <bmasney@redhat.com> 1.0
- Ensure that /etc/systemd/network directory exists

* Thu Dec 1 2022 Brian Masney <bmasney@redhat.com> 1.0
- First commit!
